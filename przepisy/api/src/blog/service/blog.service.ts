import {User} from "src/user/models/user.interface";
import {Delete, Injectable, Param} from "@nestjs/common";
import {BlogEntry} from "../models/blog-entry.interface";
import {BlogEntryEntity} from "../models/blog-entry.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {UserService} from "src/user/services/user.service";
import {from, Observable, of} from "rxjs";
import {switchMap} from "rxjs/operators";

const slugify = require("slugify");

@Injectable()
export class BlogService {
    constructor(
        @InjectRepository(BlogEntryEntity)
        private readonly blogRepository: Repository<BlogEntryEntity>,
        private readonly userService: UserService,
    ) {
    }

    create(user: User, blogEntry: BlogEntry): Observable<BlogEntry> {
        blogEntry.author = user;
        return this.generateSlug(blogEntry.title).pipe(
            switchMap((slug: string) => {
                blogEntry.slug = slug;
                return from(this.blogRepository.save(blogEntry));
            })
        );
    }

    generateSlug(title: string): Observable<string> {
        return of(slugify(title));
    }

    findAll(): Observable<BlogEntry[]> {
        return from(this.blogRepository.find(
            {
                relations: [`author`]
            }
        ));
    }

    findByUser(userId: number): Observable<BlogEntry[]> {
        return from(
            this.blogRepository.find(
                {
                    where: {
                        author: userId
                    },
                    relations: [`author`]
                }
            )
        )
    }

    findOne(id: number): Observable<BlogEntry> {
        return from(
            this.blogRepository.findOneOrFail({id}, {relations: [`author`]})
        );
    }

    updateOne(id: number, blogEntry: BlogEntry): Observable<BlogEntry> {
        return from(this.blogRepository.update(id, blogEntry))
            .pipe(
                switchMap(() => this.findOne(id))
            )
    }

    deleteOne(id: number): Observable<any> {
        return from(this.blogRepository.delete(id));
    }
}
