import {
    CanActivate,
    ExecutionContext,
    forwardRef,
    Inject,
    Injectable
} from "@nestjs/common";
import {
    Observable, of
} from "rxjs";
import { map, tap } from "rxjs/operators";
import { User } from "src/user/models/user.interface";
import {
    UserService
} from "src/user/services/user.service";


@Injectable()
export class UserIsUserGuard implements CanActivate {

    constructor(@Inject(forwardRef(() => UserService)) private userService: UserService) {
    }

    canActivate(context: ExecutionContext)
        : boolean | Promise < boolean > | Observable < boolean > {
        const request = context.switchToHttp().getRequest();
        const params = request.params;
        const user: User = request.user;
    
        return this.userService.findOne(user.id)
        .pipe(
            map((user: User) => {
                let hasPermission = false;

                return hasPermission = +params.id === user.id;
            }),
        )
    }


}
