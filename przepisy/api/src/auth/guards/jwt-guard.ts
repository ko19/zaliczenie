import { Injectable } from "@nestjs/common";
import { AuthGuard } from '@nestjs/passport'

//Użyte na kontrolerze zawsze to wykona, przed wejsciem na endpoint, sprawdza czy uzytkownik ma np. dana role

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') { }