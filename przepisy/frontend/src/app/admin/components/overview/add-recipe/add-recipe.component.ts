import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Component, OnInit} from "@angular/core";
import {UserService} from "../../../../services/user.service";

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.scss']
})
export class AddRecipeComponent implements OnInit {
  form: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService) {
  }

  public ngOnInit(): void {
    this.initForm();
  }

  public onSubmit(): void {
    console.log(this.form.getRawValue());
    this.userService.postRecipe(this.form.getRawValue()).subscribe(console.log);
  }

  private initForm(): void {
    this.form = new FormGroup({
      title: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      body: new FormControl(null, Validators.required),
      slug: new FormControl('test', [Validators.required]),
      isPublished: new FormControl(true, [Validators.required]),
      publishedDate: new FormControl(new Date(), [Validators.required])
    });
  }
}
