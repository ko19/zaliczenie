import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { BlogEntryEntity } from "./models/blog-entry.entity";
import {UserModule} from 'src/user/user.module'
import { AuthModule } from "src/auth/auth.module";
import { BlogController } from "./controller/blog.controller";
import { BlogService } from "./service/blog.service";

@Module({
  imports: [
      TypeOrmModule.forFeature([BlogEntryEntity]),
      UserModule,
      AuthModule
    ],
    controllers: [
        BlogController
    ],
    providers: [
        BlogService
    ]
  
})
export class BlogModule {}
