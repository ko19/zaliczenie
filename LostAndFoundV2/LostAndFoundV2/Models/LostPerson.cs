﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LostAndFoundV2.Models
{
    public class LostPerson
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }    
        public string Sex { get; set; }
        public DateTime LostDate { get; set; }


        public class LostPersonDBContext : DbContext
        {
            public DbSet<LostPerson> LostPersons { get; set; }
        }
    }
}