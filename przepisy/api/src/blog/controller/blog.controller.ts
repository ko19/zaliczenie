import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Request,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from "@nestjs/common";
import { Observable, of } from "rxjs";
import { JwtAuthGuard } from "src/auth/guards/jwt-guard";
import { BlogEntry } from "../models/blog-entry.interface";
import { BlogService } from "../service/blog.service";
import { UserIsAuthorGuard } from "../guards/user-is-author.guard";
import { UserIsUserGuard } from "../../auth/guards/userIsUser.guard";
import { FileInterceptor } from "@nestjs/platform-express";
import { User } from "src/user/models/user.interface";
import { diskStorage } from "multer";
import {v4 as uuidv4} from 'uuid';
import {extname, join} from "path";
import {Image} from '../models/Image.interface'

export const BLOG_ENTRIES_URL = "http://localhost:3000/blog-entries";

export const storage = {
    storage: diskStorage({
        destination: "./uploads/blog-entry-images",
        filename: (req, file, cb) => {
            const randomName = uuidv4();
            cb(null, `${randomName}${extname(file.originalname)}`);
        },
    }),
};

@Controller("blogs")
export class BlogController {
  constructor(private readonly _blogService: BlogService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() blogEntry, @Request() req): Observable<BlogEntry> {
    const user = req.user;

    return this._blogService.create(user, blogEntry);
  }

  @Get()
  getAll(): Observable<BlogEntry[]> {
    return this._blogService.findAll();
  }
  // @Get()
  // findBlogEntries(@Query(`userId`) userId: number): Observable<BlogEntry[]> {
  //     if (userId == null) {
  //         return this._blogService.findAll();
  //     }
  //     return this._blogService.findByUser(userId);
  // }

  // @Get()
  // index(@Query("page") page: number = 1, @Query("limit") limit: number = 10) {
  //   limit = limit > 100 ? 100 : limit;
  //
  //   return this._blogService.paginateAll({
  //     limit: +limit,
  //     page: +page,
  //     route: BLOG_ENTRIES_URL,
  //   });
  // }

  // @Get("user/:user")
  // indexByUser(
  //   @Query("page") page: number = 1,
  //   @Query("limit") limit: number = 10,
  //   @Param("user") userId: number,
  // ) {
  //   limit = limit > 100 ? 100 : limit;
  //
  //   return this._blogService.paginateByUser(
  //     {
  //       limit: +limit,
  //       page: +page,
  //       route: BLOG_ENTRIES_URL,
  //     },
  //     userId,
  //   );
  // }

  @Get(":id")
  findOne(@Param("id") id: number): Observable<BlogEntry> {
    return this._blogService.findOne(id);
  }

  @UseGuards(JwtAuthGuard, UserIsAuthorGuard)
  @Put(":id")
  updateOne(
    @Param("id") id: number,
    @Body() blogEntry: BlogEntry,
  ): Observable<BlogEntry> {
    return this._blogService.updateOne(+id, blogEntry);
  }

  @UseGuards(JwtAuthGuard, UserIsUserGuard)
  @Delete("id")
  deleteOne(@Param("id") id: number): Observable<any> {
    return this._blogService.deleteOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post("image/upload")
  @UseInterceptors(FileInterceptor("file", storage))
  uploadFile(@UploadedFile() file, @Request() req): Observable<Image> {
      return of(file);
  }

  @Get('image/:imagename')
  findImage(@Param("imagename") imagename, @Res() res): Observable<object> {
      return of(res.sendfile(join(process.cwd(), 'uploads/blog-entry-images/' + imagename)))
  }



}
