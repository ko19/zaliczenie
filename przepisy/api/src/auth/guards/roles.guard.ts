import { hasRoles } from './../decorator/roles.decorator';
import { Observable } from 'rxjs';
import { UserService } from './../../user/services/user.service';
import { CanActivate, ExecutionContext, forwardRef, Inject, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { User } from 'src/user/models/user.interface';
import { map } from 'rxjs/operators';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector,
        @Inject(forwardRef(() => UserService))
        private UserService: UserService) {

    }

    canActivate(context: ExecutionContext): boolean | Observable<boolean> {
        const roles = this.reflector.get<string[]>('roles', context.getHandler());

        if(!roles) {
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const user: User = request.user;

        return this.UserService.findOne(user.id)
            .pipe(
                map((user: User) => {
                    const hasRole = () => roles.indexOf(user.role) > -1;
                    let hasPermission = false;
                
                    if(hasRole()) {
                        console.log('has role true');
                        hasPermission = true;
                    }


                    return user && hasPermission;
                })
            )
        
        

    }
}
