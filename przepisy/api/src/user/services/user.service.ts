import {User, UserRole} from "src/user/models/user.interface";
import {AuthService} from "../../auth/services/auth.service";

import {UserEntity} from "./../models/user.entity";
import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Like, Repository} from "typeorm";
import {from, Observable, of} from "rxjs";
import {map, switchMap} from "rxjs/operators";
import {IPaginationOptions, paginate, Pagination,} from "nestjs-typeorm-paginate";

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        private authService: AuthService,
    ) {
    }

    create(user: User): Observable<User> {
        return this.authService.hashPassword(user.password).pipe(
            switchMap((passwordHash: string) => {
                const newUser = new UserEntity();
                newUser.email = user.email;
                newUser.name = user.name;
                newUser.username = user.username;
                newUser.password = passwordHash;
                newUser.role = UserRole.USER;

                return from(this.userRepository.save(newUser));
            }),
            map((user: User) => {
                const {password, ...result} = user;
                return result;
            }),
        );
    }

    findOne(id: number): Observable<User> {
        return from(this.userRepository.findOne(id, {relations: [`blogEntries`]})).pipe(
            map((user: User) => {
                const {password, ...result} = user;
                return result;
            }),
        );
    }

    findAll(): Observable<User[]> {
        return from(this.userRepository.find()).pipe(
            map((users: User[]) => {
                users.forEach((user) => {
                    delete user.password;
                });

                return users;
            }),
        );
    }

    paginate(options: IPaginationOptions): Observable<Pagination<User>> {
        return from(paginate<User | unknown>(this.userRepository, options)).pipe(
            map((usersPageable: Pagination<User>) => {
                usersPageable.items.forEach((user) => {
                    delete user.password;
                });
                return usersPageable;
            }),
        );
    }

    paginateFilterByUsername(
        options: IPaginationOptions,
        user: User,
    ): Observable<Pagination<User>> {
        return from(
            this.userRepository.findAndCount({
                skip: +options.page * +options.limit || 0,
                take: +options.limit || 10,
                order: {
                    id: "ASC",
                },
                select: ["id", "name", "username", "email", "role"],
                where: [
                    {
                        username: Like(`%${user.username}%`),
                    },
                ],
            }),
        ).pipe(
            map(([users, totalUsers]) => {
                const usersPage: Pagination<User> = {
                    items: users,
                    links: {
                        first: options.route + `?limit=${options.limit}`,
                        previous: options.route + ``,
                        next:
                            options.route +
                            `?limit=${options.limit}?page=${"" + options.page + 1}`,
                        last:
                            options.route +
                            `?limit=${options.limit}?page=${
                                Math.ceil(totalUsers) / +options.page
                            }`,
                    },
                    meta: {
                        currentPage: +options.page,
                        itemCount: users.length,
                        itemsPerPage: +options.limit,
                        totalItems: totalUsers,
                        totalPages: Math.ceil(totalUsers / +options.limit),
                    },
                };
                return usersPage;
            }),
        );
    }

    deleteOne(id: number): Observable<any> {
        return from(this.userRepository.delete(id));
    }

    updateOne(id: number, user: User): Observable<any> {
        delete user.email;
        delete user.password;
        delete user.role;

        return from(this.userRepository.update(id, user)).pipe(() =>
            this.findOne(id),
        );
    }

    updateRoleOfUser(id: number, user: User): Observable<any> {
        return from(this.userRepository.update(id, user));
    }

    login(user: User): Observable<string> {
        return this.validateUser(user.email, user.password).pipe(
            switchMap((user: User) => {
                if (user) {
                    return this.authService.generateJWT(user);
                } else {
                    return of(null);
                }
            }),
            map((jwt: string) => {
                if (jwt) {
                    return jwt;
                } else {
                    return "Wrong Credentials";
                }
            }),
        );
    }

    validateUser(email: string, password: string): Observable<User> {
        let requestUser: User;
        return this.findByMail(email).pipe(
            switchMap((user: User) => {
                if (user) {
                    requestUser = user;
                    return this.authService.comparePasswords(password, user.password);
                }
                return of(null);
            }),
            map((isValid: boolean) => {
                if (isValid) {
                    delete requestUser.password;
                    return requestUser;
                }
                throw new Error();
            }),
        );
    }

    findByMail(email: string): Observable<User> {
        return from(
            this.userRepository.findOne({
                email,
            }),
        );
    }
}
