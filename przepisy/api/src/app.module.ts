import { UserModule } from "./user/user.module";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { TypeOrmModule, TypeOrmModuleOptions } from "@nestjs/typeorm";
import { UserEntity } from "./user/models/user.entity";
import { AuthModule } from './auth/auth.module';
import { BlogModule } from './blog/blog.module';
import {BlogEntryEntity} from "./blog/models/blog-entry.entity";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ormConfigJson: TypeOrmModuleOptions = require("../ormconfig.json");

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot({
      ...ormConfigJson,
      keepConnectionAlive: true,
      entities: [UserEntity, BlogEntryEntity]
    }),
    UserModule,
    AuthModule,
    BlogModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {
}
