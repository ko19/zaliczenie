import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {User} from '../../models/user.model';
import {HttpErrorResponse, HttpEventType} from "@angular/common/http";
import {of} from "rxjs";


export interface File {
  data: any;
  progress: number;
  inProgress: boolean;
}

@Component({
  selector: 'app-update-user-profile',
  templateUrl: './update-user-profile.component.html',
  styleUrls: ['./update-user-profile.component.scss']
})
export class UpdateUserProfileComponent implements OnInit {

  @ViewChild('fileUpload', {static: false}) fileUpload: ElementRef;

  file: File = {
    data: null,
    inProgress: false,
    progress: 0

  }

  updateForm: FormGroup;

  constructor(private authService: AuthService,
              private formBuild: FormBuilder,
              private userService: UserService) {
  }


  ngOnInit(): void {
    this.updateForm = this.formBuild.group({
        id: [{value: null, disabled: true}, [Validators.required]],
        name: [{value: null}, [Validators.required]],
        username: [{value: null}, [Validators.required]],
        profileImage: [null]
      }
    );

    this.authService.getUserId()
      .pipe(
        mergeMap((id: number) => this.userService.findOne(id))
      ).subscribe((user: User) => this.updateForm.patchValue({
      id: user.id,
      name: user.name,
      username: user.username,
      profileImage: user.profileImage

    }));
  }

  onClick(): void {
    const fileInput = this.fileUpload.nativeElement;
    fileInput.click();
    fileInput.onchange = () => {
      this.file = {
        data: fileInput.files[0],
        inProgress: false,
        progress: 0,
      };
      this.fileUpload.nativeElement.value = '';
      this.uploadFile();
    }
  }

  uploadFile() {
    const formData = new FormData();
    formData.append('file', this.file.data);
    this.file.inProgress = true;
    this.userService.uploadProfileImage(formData)
      .pipe(
        map((event) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              this.file.progress = Math.round(event.loaded * 100 / event.total);
              break;
            case HttpEventType.Response:
              return event;
          }
        }),
        catchError((error: HttpErrorResponse) => {
          this.file.inProgress = false;
          return of('Upload Failed');
        })
      ).subscribe((event) => {
      if (typeof (event) === 'object') {
        this.updateForm.patchValue({profileImage: event.body.profileImage});
      }
    })

  }

  update(): void {
    this.userService.updateOne(this.updateForm.getRawValue()).subscribe();
  }

}
