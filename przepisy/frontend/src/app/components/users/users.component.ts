import {Component, OnInit} from '@angular/core';
import {UserData, UserService} from '../../services/user.service';
import {map} from 'rxjs/operators';
import {PageEvent} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  dataSource: UserData = null;
  displayedColumns: string[] = ['id', 'name', 'username', 'email', 'role'];
  filterValue: string = null;

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.initDataSource();
  }

  initDataSource(): void {
    this.userService.findAll(1, 10)
      .subscribe((users: UserData) => this.dataSource = users);
  }

  onPaginateChange($event: PageEvent): void {
    let page = $event.pageIndex;
    const size = $event.pageSize;
    page = page + 1;
    this.userService.findAll(page, size).pipe(
      map((users: UserData) => this.dataSource = users)
    ).subscribe();
  }

  findByName(filterValue: string): void {
    console.log(filterValue);
    this.userService.paginateByName(0, 10, filterValue)
      .subscribe((users: UserData) => this.dataSource = users);
  }

  navigateToProfile(id: number): void {
    this.router.navigate(['./' + id], {relativeTo: this.route});
  }
}
