import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {User} from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
              private jwtHelper: JwtHelperService) {
  }

  login(email: string, password: string): Observable<string> {
    return this.http.post<any>('http://localhost:3000/users/login', {email, password})
      .pipe(
        map((token) => {
          console.log(token);
          localStorage.setItem('token', token.access_token);
          return token;
        })
      );
  }

  register(user): Observable<any> {
    return this.http.post('http://localhost:3000/users', user)
      .pipe(
        map(users => users),
      );
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    console.log(token);
    return !this.jwtHelper.isTokenExpired(token);
  }

  getUserId(): Observable<any> {
    return of(localStorage.getItem('token'))
      .pipe(
        map((token: string) => this.jwtHelper.decodeToken(token)),
        map((user: any) => user.user),
        map((user: User) => user.id)
      );
  }
}
