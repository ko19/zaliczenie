import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';


export interface UserData {
  items: User[];
  meta: {
    totalItems: number;
    itemCount: number;
    itemsPerPage: number;
    totalPages: number;
    currentPage: number;
  };
  links: {
    first: string;
    previous: string;
    next: string;
    last: string;
  };
}


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  // Observable - Asynchroniczny strumien danych
  postRecipe(obj): Observable<any> {
    return this.http.post('http://localhost:3000/blogs', obj);
  }

  getAllRecipes(): Observable<any> {
    return this.http.get('http://localhost:3000/blogs');
  }


  uploadProfileImage(formData: FormData): Observable<any> {
    return this.http.post<FormData>('http://localhost:3000/users/upload', formData, {
      reportProgress: true,
      observe: "events"
    });
  }

  findOne(id: number): Observable<User> {
    return this.http.get('http://localhost:3000/users/' + id);
  }

  updateOne(user: User): Observable<User> {
    return this.http.put('http://localhost:3000/users/' + user.id, user);
  }

  findAll(page: number, size: number): Observable<UserData> {
    let params = new HttpParams();
    params = params.append('page', String(page));
    params = params.append('limit', String(size));
    return this.http.get<UserData>('http://localhost:3000/users', {params});
  }

  paginateByName(page: number, size: number, username: string): Observable<UserData> {
    let params = new HttpParams();
    params = params.append('page', String(page));
    params = params.append('limit', String(size));
    params = params.append('username', String(username));
    return this.http.get<UserData>('http://localhost:3000/users', {params});
  }
}
