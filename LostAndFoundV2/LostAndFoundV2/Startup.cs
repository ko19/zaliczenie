﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LostAndFoundV2.Startup))]
namespace LostAndFoundV2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
