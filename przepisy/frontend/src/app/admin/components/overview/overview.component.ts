import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {UserService} from "../../../services/user.service";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  recipes$: Observable<any> = of(null);

  constructor(private userService: UserService) {
  }

  //hook -> zaraz po stworzeniu(wywolaniu konstruktora to sie wywoluje)
  ngOnInit(): void {
    this.fetch();
  }

  public fetch(): void {
    this.recipes$ = this.userService.getAllRecipes();
  }


}
