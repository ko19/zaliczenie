export interface Image {
    fieldName: string;
    originalname: string;
    encoding: string;
    mimetype: string;
    description: string;
    filename: string;
    path: string;
    size: number;
}