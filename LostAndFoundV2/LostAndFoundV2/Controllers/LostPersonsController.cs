﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LostAndFoundV2.Models;

namespace LostAndFoundV2.Controllers
{
    public class LostPersonsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: LostPersons
        public ActionResult Index()
        {
            return View(db.LostPersons.ToList());
        }

       

        [Authorize(Roles ="Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LostPerson lostPerson = db.LostPersons.Find(id);
            if (lostPerson == null)
            {
                return HttpNotFound();
            }
            return View(lostPerson);
        }
        [Authorize(Roles = "Admin,User")]
        // GET: LostPersons/Create
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Surname,Sex,LostDate")] LostPerson lostPerson)
        {
            if (ModelState.IsValid)
            {
                db.LostPersons.Add(lostPerson);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lostPerson);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LostPerson lostPerson = db.LostPersons.Find(id);
            if (lostPerson == null)
            {
                return HttpNotFound();
            }
            return View(lostPerson);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Surname,Sex,LostDate")] LostPerson lostPerson)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lostPerson).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lostPerson);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LostPerson lostPerson = db.LostPersons.Find(id);
            if (lostPerson == null)
            {
                return HttpNotFound();
            }
            return View(lostPerson);
        }

        // POST: LostPersons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LostPerson lostPerson = db.LostPersons.Find(id);
            db.LostPersons.Remove(lostPerson);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
