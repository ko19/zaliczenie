import {CanActivate, ExecutionContext, Injectable} from "@nestjs/common";
import {Observable} from "rxjs";
import {UserService} from "../../user/services/user.service";
import {BlogService} from "../service/blog.service";
import {User} from "../../user/models/user.interface";
import {map, switchMap} from "rxjs/operators";
import {BlogEntry} from "../models/blog-entry.interface";


//middleware, czy uzytkownik jest autorem bloga
@Injectable()
export class UserIsAuthorGuard implements CanActivate {

    constructor(private readonly _userService: UserService,
                private readonly _blogService: BlogService) {
    }

    canActivate(context: ExecutionContext): boolean | Observable<boolean> {
        const request = context.switchToHttp().getRequest();

        const params = request.params;
        const blogEntryId = +params.id;
        const user: User = request.user;

        return this._userService.findOne(user.id)
            .pipe(
                switchMap((user: User) =>
                    this._blogService.findOne(blogEntryId)
                        .pipe(
                            map((blogEntry: BlogEntry) => {
                                let hasPermission = false;

                                if (user.id === blogEntry.author.id) {
                                    hasPermission = true;
                                }

                                return user && hasPermission;
                            })
                        )
                )
            )

    }

}
