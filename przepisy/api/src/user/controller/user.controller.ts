import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    Query,
    Request,
    Res,
    UploadedFile,
    UseGuards,
    UseInterceptors,
} from "@nestjs/common";
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {Pagination} from "nestjs-typeorm-paginate";
import {extname, join} from "path";
import {Observable, of} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {hasRoles} from "src/auth/decorator/roles.decorator";
import {JwtAuthGuard} from "src/auth/guards/jwt-guard";
import {UserIsUserGuard} from "src/auth/guards/userIsUser.guard";
import {v4 as uuidv4} from "uuid";
import {RolesGuard} from "../../auth/guards/roles.guard";
import {User, UserRole} from "../models/user.interface";
import {UserService} from "../services/user.service";

export const storage = {
    storage: diskStorage({
        destination: "./uploads",
        filename: (req, file, cb) => {
            const randomName = uuidv4();
            cb(null, `${randomName}${extname(file.originalname)}`);
        },
    }),
};

@Controller("users")
export class UserController {
    constructor(private UserService: UserService) {
    }

    @Post()
    create(@Body() user: User): Observable<User | any> {
        return this.UserService.create(user).pipe(
            map((user: User) => user),
            catchError((err) =>
                of({
                    error: err.message,
                }),
            ),
        );
    }

    @Post("login")
    login(@Body() user: User): Observable<any> {
        return this.UserService.login(user).pipe(
            map((jwt: string) => {
                return {
                    access_token: jwt,
                };
            }),
        );
    }

    @Get(":id")
    findOne(@Param() params): Observable<User> {
        return this.UserService.findOne(params.id);
    }

    @Get()
    index(
        @Query("page") page = 0,
        @Query("limit") limit = 10,
        @Query("username") username: string,
    ): Observable<Pagination<User>> {
        limit = limit > 100 ? 100 : limit;

        if (username === null || username === undefined) {
            return this.UserService.paginate({
                page,
                limit,
            });
        } else {
            return this.UserService.paginateFilterByUsername(
                {
                    page,
                    limit,
                    route: "http://localhost:3000/users",
                },
                {
                    username,
                },
            );
        }
    }

    @hasRoles(UserRole.ADMIN)
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Delete(":id")
    deleteOne(@Param("id") id: string): Observable<User> {
        return this.UserService.deleteOne(+id);
    }

    @UseGuards(JwtAuthGuard, UserIsUserGuard)
    @Put(":id")
    updateOne(@Param("id") id: string, @Body() user: User): Observable<any> {
        console.log("test");

        return this.UserService.updateOne(+id, user);
    }

    @hasRoles(UserRole.ADMIN)
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Put(":id/role")
    updateRoleOfUser(
        @Param("id") id: string,
        @Body() user: User,
    ): Observable<User> {
        return this.UserService.updateRoleOfUser(+id, user);
    }

    @UseGuards(JwtAuthGuard)
    @Post("upload")
    @UseInterceptors(FileInterceptor("file", storage))
    uploadFile(@UploadedFile() file, @Request() req): Observable<object> {
        const user: User = req.user;
        return this.UserService.updateOne(user.id, {
            profileImage: file.filename,
        }).pipe(map((user: User) => ({profileImage: user.profileImage})));
    }

    @Get("profile-image/:imagename")
    findProfileImage(
        @Param("imagename") imagename: string,
        @Res() res,
    ): Observable<object> {
        return of(res.sendfile(join(process.cwd(), "uploads/" + imagename)));
    }
}
